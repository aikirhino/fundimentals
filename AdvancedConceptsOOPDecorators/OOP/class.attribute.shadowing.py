class Point():
    x = 10
    y = 7

p = Point()
print(p.x)
print(p.y)


p.x = 12 #changes the instance attribute
print(p.x)  #prints the instance attribute
print(Point.x) #prints the class attribute

del p.x #deletes the instance attribute
print(p.x) #searches and finds the class attribute

p.z = 3 #hey it is now 3D
print(p.z)

#Point.z = 4
print(Point.z) #This will break, point has no attribute z
