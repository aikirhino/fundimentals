#Is-A or Has-A
class Engine():
    def start(self):
        pass

    def stop(self):
        pass

class ElectricEngine(Engine):
    pass

class V8Engine(Engine):
    pass

class Car():
    engine_cls = Engine

    def __init__(self):
        self.engine = self.engine_cls() #has an engine

    def start(self):
        name = self.engine.__class__.__name__
        style = self.__class__.__name__
        print(
            f'Starting engine {name} for car {style}. Wroom..wroom'
        )

        self.engine.start()

    def stop(self):
        self.engine.stop()

class RaceCar(Car): #is a car
    engine_cls = V8Engine

class CityCar(Car): #is a car
    engine_cls = ElectricEngine

class F1Car(RaceCar): #Is a racecar and also Is-a car
    engine_cls = V8Engine

car = Car()
racecar = RaceCar()
citycar = CityCar()
f1car = F1Car()
cars = [car, racecar, citycar, f1car]

for car in cars:
    car.start()
