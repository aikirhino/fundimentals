class Shape:
    geometric_type = 'Generic Shape'

    def area(self): #This acts as placeholder for the interface
        raise NotImplementedError

    def get_geometric_type(self):
        return self.geometric_type


class Plotter:
    def plot(self, ratio, topleft):
        #some nice plotting logic here
        print(f"Plotting at {topleft}, ratio {ratio} ")


class Polygon(Shape, Plotter): #base class for polygon
    geometric_type = 'Polygon'

class RegularPolygon(Polygon):
    geometric_type = 'Regular Polygon' #Is A Polygon

    def __init__(self, side):
        self.side = side

class RegularHexagon(RegularPolygon): #Is a RegularPolygon
    geometric_type = 'Regular Hexagon'

    def area(self):
        return 1.5 * (3 ** .5 * self.side ** 2)

class Square(RegularPolygon):
    geometric_type = 'Sqare'

    def area(self):
        return self.side * self.side

hexagon = RegularHexagon(10)
print(hexagon.area())
print(hexagon.get_geometric_type())
hexagon.plot(0.8, (75, 77))

square = Square(12)
print(square.area())
print(square.get_geometric_type())
square.plot(0.93, (74, 75))

print(square.__class__.__mro__)