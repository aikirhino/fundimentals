class String:

    @staticmethod
    def is_palindrome(s, case_insensitive=True):
        s = ''.join(c for c in s if c.isalnum()) #study this
        #For case insensitive comparison, we loswer case s
        if case_insensitive:
            s = s.lower()
        for c in range(len(s) // 2):
            if s[c] != s[-c -1]:
                return False
    @staticmethod
    def get_unique_words(sentence):
        return set(sentence.split())


print(String.get_unique_words(
    'i love palindromes.  I really really love them!'))