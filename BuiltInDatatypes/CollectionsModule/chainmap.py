from collections import ChainMap
default_collection = {'host': 'localhost', 'port': 4567}

connection = {'port': 5678}
conn = ChainMap(connection, default_collection)
print(conn['port'])
print(conn['host'])

print(conn.maps)
conn['host'] = 'usfholland.com'
print(conn.maps)

