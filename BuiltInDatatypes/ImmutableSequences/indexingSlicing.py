def aStr(s):
    print(s)


s = "The trouble is that you think you have time!"

aStr(s[0]) #prints T
aStr(s[5]) #prints r

aStr(s[:4]) #prints from index 0 to 4 [0][1][3]

aStr(s[4:]) #prints from index 5 to the end of the string

aStr(s[2:14:3]) #slices the strong

aStr(s[:]) #makes a copy
