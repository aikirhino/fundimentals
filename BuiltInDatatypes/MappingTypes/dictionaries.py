a = dict(A=1, Z=-1)
b = {'A': 1, 'Z': -1}
c = dict(zip(['A', 'Z'], [1, -1]))
d = dict([('A', 1), ('Z', -1)])
e = dict({'Z': -1, 'A':1})

print(a == b == c == d == e)

#side not about zip
print(list(zip(['h','e','l','l','o'],[1,2,3,4,5]))) #like a zipper

d = {}
d['a'] = 1
d['b'] = 2
print(d)
d['c'] =  3
print('c' in d) #looks up by key, not value
print(3 in d)

e = dict(zip('hello', range(50)))
print(e)
print(e.keys())
print(e.values())

print(3 in e.values())

print(('o', 4) in e.items())
print(e)
print(e.popitem())
# print(e.pop(1))
e.update({'another': 'value'})
print(e)
e.update(a=13)
print(e)
print(e.get('a'))