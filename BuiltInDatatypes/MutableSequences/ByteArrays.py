def aBa(a):
    print(a)

a = bytearray()  #empty object
aBa(a)

b = bytearray(10) #zero-filled instance of a given length
aBa(b)

c = bytearray(range(5))
aBa(c)

name = bytearray(b'Lina')  #A - bytearray from bytes
aBa(name)
aBa(name.replace(b'L', b'l'))  #find replace


