def aList(l):
    print(l)

aList([])

#[] and list() is the same

a = [1, 2, 3] #elements are seperated by comma
aList(a)

b = [x ** 5 for x in [2, 3, 5]]  #python magic
aList(b)

a_tuple = (1, 3, 5, 7, 9)
c = list((a_tuple))  #making a list from a tuple
aList(c)

a_string = 'hello'
d = list(a_string) #a list from a string index
aList(d)

a.append(13) #can append any object onto the end
aList(a)

aList(a.count(1)) #count matching items in list

a.extend([5, 7]) #extend the list by another (or squence)
aList(a)

a.insert(0, 17) #inserts at index 0
aList(a)

aList(a.pop()) #pop (remove and return) last element
aList(a.pop(3)) #pops element at index 3

b = list('hello')
aList(b)
b.append(100)
aList(b)
b.extend((1, 2, 3))
aList(b)
b.extend('...')
aList(b)

aList(min(a)) #minimum value in the list
aList(max(a)) #max value
aList(sum(a)) #sum of all values
aList(len(a)) #length of collection

c = a + b #concatenate elements from both
aList(c)

d = a * 2 #concatenates the list to itself
aList(d)

from operator import itemgetter
e = [(5, 3), (1, 3), (1, 2), (2, -1), (4, 9)]
aList(e)
aList(sorted(e))
aList(sorted(e, key=itemgetter(0)))
aList(sorted(e, key=itemgetter(0, 1)))
aList(sorted(e, key=itemgetter(1)))
aList(sorted(a, key=itemgetter(1), reverse=True))
