a = int(True) #True behaves like 1
print(a)

b = int(False) #False behaves like 0
print(b)

c = bool(1) # 1 evaluates to True in a boolean context
print(c)

d = bool(-42) #so does every non-zero number
print(d)

e = bool(0) #evaluates to False
print(e)

f = not True #evaluates to False
print(f)

g = not False #evaluates to True
print(g)

h = True and True #evaluates to True
print(h)

i = False or True #evaluates to True
print(i)

j = 1 + True #evaluates to 2
print(j)

k = False + 42 #evaluates to 42
print(k)

l = 7 - True #evaluates to 6
print(l)



