#use of imagary numbers

def aNum(n):
    print(n)


c = 3.14 + 2.73j
aNum(c.real) #it is real
aNum(c.imag) #imaginary part
aNum(c.conjugate()) #conjugate of N + Kj is A = Kj

#mulitplication is allowed
aNum(c * 2)

#power equations
aNum(c ** 2)

#addition
d = 1 + 1j
aNum(c + d)