#Integers have an unlimited range, subject only to available memory.

def aNum(n):
    print(n)


a = 12
aNum(a)

b = 3
aNum(b)

c = a + b #prints 15
aNum(c)

d = b - a #print -9
aNum(d)

e = a // b #integer division.. floors to the nearest integer
aNum(e)

f = a / b #true division
aNum(f)

g = a * b #multiplication
aNum(g) #power fucntion

h = 2 ** 1024  #python can handle very big numbers. Prints:
# 17976931348623159077293051907890247336179769789423065
# 72734300811577326758055009631327084773224075360211201
# 13879871393357658789768814416622492847430639474124377
# 76789342486548527630221960124609411945308295208500576
# 88381506823424628814739131105408272371633505106845862
# 98239947245938479716304835356329624224137216

aNum(h)

i = int(1.75) #prints 1
aNum(i)

j = int(-1.75) #prints -1
aNum(j)
#truncation is down towards 0

k = 10 % 3 # remainder of the division of 10 // 3
aNum(k)

