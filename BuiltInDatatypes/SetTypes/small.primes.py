#Hashability is a characteristic that allows an object to be used as a set member
#as well as a key for a dictionary,
#as we'll see very soon.

def aSet(s):
    print(s)

small_primes = set()  #empty set
small_primes.add(2)
small_primes.add(3)
small_primes.add(5)

aSet(small_primes)

small_primes.add(1) #oh no, 1 is not a prime
aSet(small_primes)

small_primes.remove(1)
aSet(small_primes)

aSet(3 in small_primes)
small_primes.add(3)
aSet(small_primes) #duplicates are not allowed

#another way to create a set
smaller_primes = {7, 11, 13, 17}
aSet(smaller_primes)
aSet(type(smaller_primes))