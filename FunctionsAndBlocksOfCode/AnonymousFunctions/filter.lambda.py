def get_multiples_of_give(n):
    return list(filter(lambda k: not k % 513, range(1, n+1)))

print(get_multiples_of_give(23532))