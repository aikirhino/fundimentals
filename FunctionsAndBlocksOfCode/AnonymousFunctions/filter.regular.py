def is_multiple_of_five(n):
    return not n % 5

def get_multiple_of_give(n):
    return list(filter(is_multiple_of_five, range(n)))

print(get_multiple_of_give(50))