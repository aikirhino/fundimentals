#example 1: adder
def adder(a, b):
    return a + b

#lambda equivent:
adder_lambda = lambda a, b: a + b

print("function ", adder(5, 6))
print("lambda ", adder_lambda(5,6))

#example 2: to uppercase
def to_upper(s):
    return s.upper()

s = 'abcdefghijklmnopqustuvwxy'

#lambda equivalent
to_upper_lambda = lambda s: s.upper()

print("function ", to_upper(s))
print("lambda ", to_upper_lambda(s))