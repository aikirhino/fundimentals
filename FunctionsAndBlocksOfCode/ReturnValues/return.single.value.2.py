from functools import reduce
from operator import mul

def factorial(n):
    return reduce(mul, range(1, n + 1), 1)

f5 = factorial(5)
print(f5)
#https://docs.python.org/3/library/functools.html