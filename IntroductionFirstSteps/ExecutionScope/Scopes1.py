# The local scope, which is the innermost one and contains local names.
# The enclosing scope, that is, the scope of any enclosing function.
# The global scope contanis the global names
# The built-in scope contains the built-in names. Python comes with a set of function that you can use in a
# off-the-shelf fashion, such as print, all, abs, and so on.

# name... Any object or variable.


#order of namespace scanned: Local, Enclosing, Global, Built-in


def local():
    m = 7 #local
    print(m)

m = 5  #global
print(m)

local()