#Local, Enclosing and Global

def enclosing_func():
    m = 13
    def local():
        # m doesn't belong to the scope defined by the local
        # function so Python will keep looking
        print(m, 'printing from the local scope')
    local()
m = 5
print(m, 'printing from the global scope')

enclosing_func()