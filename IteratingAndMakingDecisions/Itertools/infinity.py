from itertools import count
for n in count(5, 3):
    if n > 20000:
        break
    print(n, end=', ')
