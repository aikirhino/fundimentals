#convert into into binary.
n = 292290292304208924
remainders = []
while n > 0:
    remainder = n % 2 #get remainder
    remainders.append(remainder) #track remainder
    n //= 2 #divide by half

remainders = remainders[::-1]
print("".join(str(x) for x in remainders))