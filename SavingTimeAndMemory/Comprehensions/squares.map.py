#If you code like this you're not a python person!!!


squares = []
for n in range(10):
    squares.append(n ** 2)

print("bad python: ", list(squares))

squares = map(lambda n: n**2, range(10))

print("better python: ", list(squares))


