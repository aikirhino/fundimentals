def adder(*n):
    return sum(n)

s1 = sum(map(lambda n: adder(*n), zip(range(100), range(1, 101)))) #lambda

s2 = sum(adder(*n) for n in zip(range(100), range(1, 101))) #generator

print(s1)
print(s2)