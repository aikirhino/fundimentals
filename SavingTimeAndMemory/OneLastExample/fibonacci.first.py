def fibonacci(N):
    """Return all fibonacci number up to N"""
    result = [0]
    next_n =1
    while next_n <= N:
        result.append(next_n)
        next_n = sum(result[-2:])
    return result

print(fibonacci(0))
print(fibonacci(1))
print(fibonacci(10))
print(fibonacci(50))
print(fibonacci(100))
print(fibonacci(150))
print(fibonacci(5000))
print(fibonacci(500000))