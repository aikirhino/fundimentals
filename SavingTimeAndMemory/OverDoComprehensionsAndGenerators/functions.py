def gcd(a, b):
    """Calculate the greatest common divisor of (a, b)"""
    while b != 0:
        a, b = b, a % b
    return a
