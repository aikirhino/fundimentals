class ExitloopException(Exception):
    pass

try:
    n = 100
    found = False
    for a in range(n):
        if found: break
        for b in range(n):
            if found: break
            for c in range(n):
                if found: break
                if 42 * a + 17 * b + c == 5096:
                    found = True
                    print(a, b, c)
except ExitloopException as ele:
    print(ele)