from unittest import TestCase
from nose.tools import assert_list_equal
import sys
sys.path.insert(0, "/home/ryan/dev/sandbox/fundimentals/TestingProfilingExceptions/Testing")
from filter_funcs import filter_ints

class FilterIntsTestCase(TestCase):
    def test_filter_ints_resturn_value(self):
        v = [3, -4, 0, -2, 5, 0, 8, -1]
        result = filter_ints(v)
        assert_list_equal([3, 5, 8], result)

